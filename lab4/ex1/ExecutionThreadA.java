package edu.utcn.str.lab4.ex1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ExecutionThreadA extends Thread
{
    Integer monitor;
    int sleep_min, sleep_max, activity_min, activity_max;

    public ExecutionThreadA(Integer monitor, int sleep_min, int sleep_max, int activity_min, int activity_max)
    {
        this.monitor = monitor;
        this.sleep_min = sleep_min;
        this.sleep_max = sleep_max;
        this.activity_min = activity_min;
        this.activity_max = activity_max;
    }

    public void run() {
        System.out.println(this.getName() + " - STATE 1");

        synchronized (monitor) {
            System.out.println(this.getName() + " - STATE 2");
            int k = (int) Math.round(Math.random()*(activity_max
                    - activity_min) + activity_min);
            for (int i = 0; i < k * 100000; i++) {
                i++; i--;
            }

            try {
                Thread.sleep(Math.round(Math.random() * (sleep_max
                        - sleep_min)+ sleep_min) * 500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            System.out.println(this.getName() + " - STATE 3");
        }

    }

    public static void main(String[] args)
    {
        Integer P9 = new Integer(1);
        Integer P10 = new Integer(1);


        new ExecutionThreadA(P9, 4, 4, 2, 4).start();
        new ExecutionThreadB(P9, P10, 3, 3, 3, 6).start();
        new ExecutionThreadA(P10, 5, 5, 2, 5).start();

    }
}
