
public class ExecutionThread extends Thread {
    Integer monitor;
    Integer monitor2;
    int sleep_min, sleep_max, activity_min, activityTime;

    public ExecutionThread(Integer monitor, int sleep_min, int sleep_max, int activityTime) {
        this.monitor = monitor;
        this.sleep_min = sleep_min;
        this.sleep_max = sleep_max;
        this.activityTime = activityTime;
    }

    public ExecutionThread(Integer monitor, Integer monitor2, int sleep_min, int sleep_max, int activityTime) {
        this.monitor = monitor;
        this.monitor = monitor2;
        this.sleep_min = sleep_min;
        this.sleep_max = sleep_max;
        this.activityTime = activityTime;

    }

    public void run() {
        System.out.println(this.getName() + " - STATE 1");
        try {
            Thread.sleep(Math.round(Math.random() * (sleep_max
                    - sleep_min) + sleep_min) * 500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(this.getName() + " - STATE 2");
        if (monitor2 == null) {
            synchronized (monitor) {
                System.out.println(this.getName() + " - STATE 3");
                int k = (int) Math.round(Math.random() * activityTime);
                for (int i = 0; i < k * 100000; i++) {
                    i++;
                    i--;
                }
            }
        } else
            synchronized (monitor) {
                synchronized (monitor2) {
                    System.out.println(this.getName() + " - STATE 3");
                    int k = (int) Math.round(Math.random() * activityTime);
                    for (int i = 0; i < k * 100000; i++) {
                        i++;
                        i--;
                    }
                }
            }

        System.out.println(this.getName() + " - STATE 4");
    }
}